"Noice colorscheme
colorscheme desert

"Syntax highlighting
syntax on

"Tab width
filetype plugin indent on
set tabstop=4 softtabstop=4 shiftwidth=4 expandtab 

"Preserve visual block after indentation
vnoremap > >gv
vnoremap < <gv

"Show at least 15 lines above/below cursor
set scrolloff=15


